<?php

declare(strict_types=1);

namespace Drupal\Tests\domain_entity\Functional;

use Drupal\domain_access\DomainAccessManager;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Url;
use Drupal\domain_entity\DomainEntityMapper;
use Drupal\domain_entity\DomainEntitySourceMapper;
use Drupal\Tests\domain\Functional\DomainTestBase;
use Drupal\Tests\taxonomy\Traits\TaxonomyTestTrait;
use function array_keys;
use function array_merge;
use function drupal_flush_all_caches;

/**
 * Tests behavior for getting all URLs for an entity.
 *
 * Inspired by \Drupal\Tests\domain_source\Functional
 * \DomainSourceContentUrlsTest.
 *
 * @group domain_entity
 */
class DomainEntitySourceUrlsTest extends DomainTestBase {

  use TaxonomyTestTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'domain',
    'domain_access',
    'domain_entity',
    'field',
    'taxonomy',
  ];

  /**
   * {@inheritdoc}
   */
  protected $profile = 'minimal';

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * The vocabulary used for creating terms.
   *
   * @var \Drupal\taxonomy\VocabularyInterface
   */
  protected $vocabulary;

  /**
   * The taxonomy term used for this test.
   *
   * @var \Drupal\taxonomy\TermInterface
   */
  protected $term;

  /**
   * {@inheritdoc}
   *
   * @var \Drupal\domain_entity\DomainEntitySourceMapper
   */
  protected $sourceMapper;

  /**
   * {@inheritdoc}
   *
   * @var \Drupal\domain\DomainStorageInterface
   */
  protected $domainStorage;

  /**
   * {@inheritdoc}
   *
   * @var string
   */
  protected $domainSourceId;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->vocabulary = $this->createVocabulary();
    // Create 4 domains.
    $this->domainCreateTestDomains(4);
    $this->sourceMapper = $this->container->get('domain_entity.source_mapper');
    $this->domainStorage = $this->container->get('entity_type.manager')->getStorage('domain');

    $this->container->get('domain_entity.mapper')->createFieldStorage('taxonomy_term');
    $this->container->get('domain_entity.mapper')->addDomainField('taxonomy_term', $this->vocabulary->id());
    $this->sourceMapper->addDomainSourceField('taxonomy_term', $this->vocabulary->id());

    $this->domainSourceId = 'one_example_com';
    $values = [
      DomainEntityMapper::FIELD_NAME =>
      ['example_com', 'one_example_com', 'two_example_com'],
      DomainEntitySourceMapper::FIELD_NAME => $this->domainSourceId,
    ];
    $this->term = $this->createTerm($this->vocabulary, $values);
  }

  /**
   * Builds a list of all possibles urls for the given entity.
   *
   * @param \Drupal\Core\Entity\FieldableEntityInterface $entity
   *   The FieldableEntityInterface.
   *
   * @return string[]
   *   Return an array of string
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  protected function getEntityUrls(FieldableEntityInterface $entity): array {
    $list = [];
    $processed = FALSE;
    $domains = DomainAccessManager::getAccessValues($entity, DomainEntityMapper::FIELD_NAME);
    if ($entity->hasField(DomainEntitySourceMapper::FIELD_NAME)) {
      $source = $this->sourceMapper::getSourceDomain($entity);
      if (isset($domains[$source])) {
        unset($domains['source']);
      }
      if (!empty($source)) {
        $list[] = $source;
      }
      $processed = TRUE;
    }
    $list = array_merge($list, array_keys($domains));
    $domains = $this->domainStorage->loadMultiple($list);
    $urls = [];
    foreach ($domains as $domain) {
      $options['domain_target_id'] = $domain->id();
      $url = $entity->toUrl('canonical', $options);
      if ($processed) {
        $urls[$domain->id()] = $url->toString();
      }
      else {
        $urls[$domain->id()] = $domain->buildUrl($url->toString());
      }
    }
    return $urls;
  }

  /**
   * Tests the domain source URLs functionality for the.
   *
   * Entity type taxonomy term.
   */
  public function testTaxonomyTermCanonicalUrlsRewrite(): void {
    $domainSourceId = $this->domainSourceId;
    $term = $this->term;

    // Variables for our tests.
    $entity_type_id = 'taxonomy_term';
    $entity_id = '1';
    $path = 'taxonomy/term/1';
    /** @var \Drupal\domain\DomainInterface[] $domains */
    $domains = $this->domainStorage->loadMultiple();
    $source = $domains[$domainSourceId];
    $expected = $source->getPath() . $path;
    $route_name = "entity.$entity_type_id.canonical";
    $route_parameters = [$entity_type_id => $entity_id];
    $uri = "entity:$entity_type_id/$entity_id";
    $uri_path = '/' . $path;
    $options = [];

    // Get the link using Url::fromRoute().
    $url = Url::fromRoute($route_name, $route_parameters, $options)->toString();
    $this->assertSame($expected, $url, 'fromRoute');

    // Get the link using Url::fromUserInput()
    $url = Url::fromUserInput($uri_path, $options)->toString();
    $this->assertSame($expected, $url, 'fromUserInput');

    // Get the link using Url::fromUri()
    $url = Url::fromUri($uri, $options)->toString();
    $this->assertSame($expected, $url, 'fromUri');

    $urls = $this->getEntityUrls($term);
    $expected = [
      $domainSourceId => $domains[$domainSourceId]->getPath() . $path,
      'example_com' => $domains['example_com']->getPath() . $path,
      'two_example_com' => $domains['two_example_com']->getPath() . $path,
    ];

    $this->assertSame($expected, $urls);
  }

  /**
   * {@inheritdoc}
   */
  public function testDisabledRoute(): void {
    $path = 'taxonomy/term/1/edit';
    /** @var \Drupal\domain\DomainInterface[] $domains */
    $domains = $this->domainStorage->loadMultiple();
    $source = $domains[$this->domainSourceId];
    $expected = $source->getPath() . $path;
    // Get the link using Url::fromUserInput()
    $url = Url::fromUserInput('/' . $path, [])->toString();
    $this->assertSame($expected, $url, 'fromUserInput enabled route rewrite');

    $field = $this->sourceMapper->loadField('taxonomy_term', $this->vocabulary->id());
    $field->setThirdPartySetting('domain_entity', 'exclude_routes', ['edit_form']);
    $field->save();
    // @todo Find out which cache needs to cleared.
    drupal_flush_all_caches();
    $expected = '/' . $path;
    $url = Url::fromUserInput('/' . $path)->toString();
    $this->assertSame($expected, $url, 'fromUserInput disabled route rewrite');
  }

}
