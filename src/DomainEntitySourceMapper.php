<?php

declare(strict_types=1);

namespace Drupal\domain_entity;

use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\field\Entity\FieldConfig;

/**
 * Provides fields operations for domain entity module fields.
 */
final class DomainEntitySourceMapper extends DomainEntityMapper {

  /**
   * The name of the domain source field.
   */
  const FIELD_NAME = 'domain_source';

  /**
   * Creates domain source field.
   *
   * @param string $entity_type
   *   The entity type machine name.
   * @param string $bundle
   *   The entity type's bundle.
   */
  public function addDomainSourceField(string $entity_type, string $bundle): void {
    $field_storage = $this->createFieldStorage($entity_type);
    $field = FieldConfig::loadByName($entity_type, $bundle, self::FIELD_NAME);
    if ($field === NULL) {
      $field = [
        'label' => 'Domain Source',
        // @todo Add better naming for entities without bundles.
        'description' => 'Select the canonical domain for this entity.',
        'bundle' => $bundle,
        'required' => FALSE,
        'field_storage' => $field_storage,
        'default_value_callback' => 'domain_entity_field_default_domains',
      ];

      $field = $this->entityTypeManager->getStorage('field_config')
        ->create($field);
      $field->save();

      // Assign widget settings for the 'default' form mode.
      $entity_form_display = $this->entityTypeManager->getStorage('entity_form_display')->load($entity_type . '.' . $bundle . '.default');
      if ($entity_form_display) {
        /** @var \Drupal\Core\Entity\Display\EntityFormDisplayInterface $entity_form_display */
        $entity_form_display->setComponent(self::FIELD_NAME, [
          'type' => 'options_select',
        ])->save();
      }

      // Assign display settings for the 'default' view mode.
      $entity_view_display = $this->entityTypeManager->getStorage('entity_view_display')->load($entity_type . '.' . $bundle . '.default');
      if ($entity_view_display) {
        /** @var \Drupal\Core\Entity\Display\EntityViewDisplayInterface $entity_view_display */
        $entity_view_display->removeComponent(self::FIELD_NAME)->save();
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function createFieldStorage($entity_type_id) {
    if ($field_storage = $this->loadFieldStorage($entity_type_id)) {
      // Prevent creation of existing field storage.
      return $field_storage;
    }
    $storage = $this->entityTypeManager->getStorage('field_storage_config');
    $field_storage = $storage->create([
      'entity_type' => $entity_type_id,
      'field_name' => static::FIELD_NAME,
      'type' => 'entity_reference',
      // @todo Polish to enable UI and optimize storage.
      'persist_with_no_fields' => TRUE,
      'locked' => FALSE,
    ]);
    $field_storage
      ->setSetting('target_type', 'domain')
      ->save();
    return $field_storage;
  }

  /**
   * Returns the source domain associated to the given entity.
   *
   * @param \Drupal\Core\Entity\FieldableEntityInterface $entity
   *   The entity to check.
   *
   * @return string|null
   *   The value assigned to the entity, either a domain id string or NULL.
   */
  public static function getSourceDomain(FieldableEntityInterface $entity): ?string {
    $source = NULL;

    if (!$entity->hasField(self::FIELD_NAME)) {
      return $source;
    }

    $field = $entity->get(self::FIELD_NAME);
    if (!$field->isEmpty()) {
      $target_id = $field->first()->get('target_id')->getString();
      if ($domain = \Drupal::entityTypeManager()->getStorage('domain')->load($target_id)) {
        $source = $domain->id();
      }
    }
    return $source;
  }

}
