<?php

declare(strict_types=1);

namespace Drupal\domain_entity\HttpKernel;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\PathProcessor\OutboundPathProcessorInterface;
use Drupal\domain\DomainInterface;
use Drupal\domain\DomainNegotiatorInterface;
use Drupal\domain_entity\DomainEntitySourceMapper;
use Drupal\Core\Render\BubbleableMetadata;
use Drupal\Core\Url;
use Drupal\path_alias\AliasManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use function array_flip;
use function end;
use function explode;
use function is_array;

/**
 * Processes the outbound path using path alias lookups.
 */
class DomainEntitySourcePathProcessor implements OutboundPathProcessorInterface {

  /**
   * The Domain negotiator.
   *
   * @var \Drupal\domain\DomainNegotiatorInterface
   */
  protected $negotiator;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The path alias manager.
   *
   * @var \Drupal\path_alias\AliasManagerInterface
   */
  protected $aliasManager;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * An array of content entity types.
   *
   * @var array
   */
  protected $entityTypes;

  /**
   * An array of routes exclusion settings, keyed by route.
   *
   * @var array
   */
  protected $excludedRoutes;

  /**
   * The active domain request.
   *
   * @var \Drupal\domain\DomainInterface
   */
  protected $activeDomain;

  /**
   * The domain storage.
   *
   * @var \Drupal\domain\DomainStorageInterface|null
   */
  protected $domainStorage;

  /**
   * The FieldableEntityInterface.
   *
   * @var \Drupal\Core\Entity\FieldableEntityInterface
   */
  protected $entity;

  /**
   * The domain entity source mapper.
   *
   * @var \Drupal\domain_entity\DomainEntitySourceMapper
   */
  protected $sourceMapper;

  /**
   * Constructs a DomainSourcePathProcessor object.
   *
   * @param \Drupal\domain\DomainNegotiatorInterface $negotiator
   *   The domain negotiator.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\path_alias\AliasManagerInterface $alias_manager
   *   The path alias manager.
   * @param \Drupal\domain_entity\DomainEntitySourceMapper $source_mapper
   *   The domain entity source mapper.
   */
  public function __construct(DomainNegotiatorInterface $negotiator, ModuleHandlerInterface $module_handler, EntityTypeManagerInterface $entity_type_manager, AliasManagerInterface $alias_manager, DomainEntitySourceMapper $source_mapper) {
    $this->negotiator = $negotiator;
    $this->moduleHandler = $module_handler;
    $this->entityTypeManager = $entity_type_manager;
    $this->aliasManager = $alias_manager;
    $this->sourceMapper = $source_mapper;
  }

  /**
   * {@inheritdoc}
   */
  public function processOutbound($path, &$options = [], ?Request $request = NULL, ?BubbleableMetadata $bubbleable_metadata = NULL) {
    // Load the active domain if not set.
    if (empty($options['active_domain'])) {
      $options['active_domain'] = $this->getActiveDomain();
    }

    // Only act on valid internal paths and when a domain loads.
    if (!$options['active_domain'] instanceof DomainInterface || empty($path) || !empty($options['external'])) {
      return $path;
    }

    // Set the default source information.
    $source = NULL;

    // Get the current language.
    $langcode = NULL;
    if (!empty($options['language']) && $options['language'] instanceof LanguageInterface) {
      $langcode = $options['language']->getId();
    }

    // Get the URL object for this request.
    $alias = $this->aliasManager->getPathByAlias($path, $langcode);
    $url = Url::fromUserInput($alias, $options);

    // Check the route, if available. Entities can be configured to
    // only rewrite specific routes.
    if ($url->isRouted()) {
      // Load the entity to check.
      if (!empty($options['entity'])) {
        $this->entity = $options['entity'];
      }
      else {
        $parameters = $url->getRouteParameters();
        if (!empty($parameters)) {
          $this->entity = $this->getEntity($parameters);
        }
      }
    }

    // One hook for entities.
    if ($this->entity instanceof FieldableEntityInterface && $url->isRouted() && $this->allowedRoute($url->getRouteName())) {
      $entity = $this->entity;
      // Ensure we send the right translation.
      if (!empty($langcode) && method_exists($entity, 'hasTranslation') && $entity->hasTranslation($langcode) && $translation = $entity->getTranslation($langcode)) {
        $entity = $translation;
      }
      if (isset($options['domain_target_id'])) {
        $target_id = $options['domain_target_id'];
      }
      else {
        $target_id = DomainEntitySourceMapper::getSourceDomain($entity);
      }
      if (!empty($target_id)) {
        $source = $this->domainStorage()->load($target_id);
      }
      $options['entity'] = $entity;
      $options['entity_type'] = $entity->getEntityTypeId();
      $this->moduleHandler->alter('domain_entity_source', $source, $path, $options);
    }
    // One for other, because the latter is resource-intensive.
    elseif ($url->isRouted()) {
      if (isset($options['domain_target_id'])) {
        $target_id = $options['domain_target_id'];
        $source = $this->domainStorage()->load($target_id);
      }
      $this->moduleHandler->alter('domain_entity_source_path', $source, $path, $options);
    }
    // If a source domain is specified, rewrite the link.
    if ($source instanceof DomainInterface) {
      // Note that url rewrites add a leading /, which getPath() also adds.
      $options['base_url'] = trim($source->getPath(), '/');
      $options['absolute'] = TRUE;
    }
    return $path;
  }

  /**
   * Derive entity data from a given route's parameters.
   *
   * @param array $parameters
   *   An array of route parameters.
   *
   * @return \Drupal\Core\Entity\EntityInterface|null
   *   Returns the entity when available, otherwise NULL.
   */
  public function getEntity(array $parameters): ?EntityInterface {
    $entity = NULL;
    $entity_types = $this->getEntityTypes();
    foreach ($parameters as $entity_type => $value) {
      if (!empty($entity_type) && isset($entity_types[$entity_type])) {
        $entity = $this->entityTypeManager->getStorage($entity_type)->load($value);
      }
    }
    return $entity;
  }

  /**
   * Gets an array of content entity types, keyed by type.
   *
   * @return \Drupal\Core\Entity\EntityTypeInterface[]
   *   An array of content entity types, keyed by type.
   */
  public function getEntityTypes() {
    if (!isset($this->entityTypes)) {
      foreach ($this->entityTypeManager->getDefinitions() as $type => $definition) {
        if ($definition->getGroup() === 'content') {
          $this->entityTypes[$type] = $type;
        }
      }
    }
    return $this->entityTypes;
  }

  /**
   * Checks that a route's common name is not disallowed.
   *
   * Looks at the name (e.g. canonical) of the route without regard for
   * the entity type.
   *
   * @parameter $name
   *   The route name being checked.
   *
   * @return bool
   *   Returns TRUE when allowed, otherwise FALSE.
   */
  public function allowedRoute($name) {
    $excluded = $this->getExcludedRoutes();
    $parts = explode('.', $name);
    $route_name = end($parts);
    // Config is stored as an array. Empty items are not excluded.
    return !isset($excluded[$route_name]);
  }

  /**
   * Gets the settings for domain source path rewrites.
   *
   * @return array
   *   The settings for domain source path rewrites.
   */
  public function getExcludedRoutes() {
    if (!isset($this->excludedRoutes)) {

      $field = $this->sourceMapper->loadField($this->entity->getEntityTypeId(), $this->entity->bundle());
      $settings = $field ? $field->getThirdPartySettings('domain_entity') : [];
      $settings += [
        'exclude_routes' => [],
      ];
      if (is_array($settings['exclude_routes'])) {
        $this->excludedRoutes = array_flip($settings['exclude_routes']);
      }
      else {
        $this->excludedRoutes = [];
      }
    }
    return $this->excludedRoutes;
  }

  /**
   * Gets the active domain.
   *
   * @return \Drupal\domain\DomainInterface
   *   The active domain.
   */
  public function getActiveDomain() {
    if (!isset($this->activeDomain)) {
      // Ensure that the loader has run.
      // In some tests, the kernel event has not.
      $active = $this->negotiator->getActiveDomain();
      if (empty($active)) {
        $active = $this->negotiator->getActiveDomain(TRUE);
      }
      $this->activeDomain = $active;
    }
    return $this->activeDomain;
  }

  /**
   * Retrieves the domain storage handler.
   *
   * @return \Drupal\domain\DomainStorageInterface
   *   The domain storage handler.
   */
  protected function domainStorage() {
    if (!$this->domainStorage) {
      $this->domainStorage = $this->entityTypeManager->getStorage('domain');
    }
    return $this->domainStorage;
  }

}
